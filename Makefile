all: main

run:
	@./main -g generos.csv -p pessoas.csv -m midias.csv -e emprestimos.csv

clean:
	@rm -rf bin main

#
# Classes do nemo-utils-cpp:
#
PACOTE_UTILS=bin/br_ufes_inf_nemo_cpp_util/DateUtils.o bin/br_ufes_inf_nemo_cpp_util/NumberUtils.o bin/br_ufes_inf_nemo_cpp_util/NumPunctPTBR.o bin/br_ufes_inf_nemo_cpp_util/StringUtils.o bin/br_ufes_inf_nemo_cpp_util/Tokenizer.o

bin/br_ufes_inf_nemo_cpp_util:
	mkdir -p bin/br_ufes_inf_nemo_cpp_util

bin/br_ufes_inf_nemo_cpp_util/DateUtils.o: bin/br_ufes_inf_nemo_cpp_util src/br_ufes_inf_nemo_cpp_util/DateUtils.cpp src/br_ufes_inf_nemo_cpp_util/DateUtils.h
	g++ -std=c++11 -o bin/br_ufes_inf_nemo_cpp_util/DateUtils.o -c src/br_ufes_inf_nemo_cpp_util/DateUtils.cpp

bin/br_ufes_inf_nemo_cpp_util/NumberUtils.o: bin/br_ufes_inf_nemo_cpp_util src/br_ufes_inf_nemo_cpp_util/NumberUtils.cpp src/br_ufes_inf_nemo_cpp_util/NumberUtils.h src/br_ufes_inf_nemo_cpp_util/NumPunctPTBR.h
	g++ -std=c++11 -o bin/br_ufes_inf_nemo_cpp_util/NumberUtils.o -c src/br_ufes_inf_nemo_cpp_util/NumberUtils.cpp

bin/br_ufes_inf_nemo_cpp_util/NumPunctPTBR.o: bin/br_ufes_inf_nemo_cpp_util src/br_ufes_inf_nemo_cpp_util/NumPunctPTBR.cpp src/br_ufes_inf_nemo_cpp_util/NumPunctPTBR.h
	g++ -std=c++11 -o bin/br_ufes_inf_nemo_cpp_util/NumPunctPTBR.o -c src/br_ufes_inf_nemo_cpp_util/NumPunctPTBR.cpp

bin/br_ufes_inf_nemo_cpp_util/StringUtils.o: bin/br_ufes_inf_nemo_cpp_util src/br_ufes_inf_nemo_cpp_util/StringUtils.cpp src/br_ufes_inf_nemo_cpp_util/StringUtils.h
	g++ -std=c++11 -o bin/br_ufes_inf_nemo_cpp_util/StringUtils.o -c src/br_ufes_inf_nemo_cpp_util/StringUtils.cpp

bin/br_ufes_inf_nemo_cpp_util/Tokenizer.o: bin/br_ufes_inf_nemo_cpp_util src/br_ufes_inf_nemo_cpp_util/Tokenizer.cpp src/br_ufes_inf_nemo_cpp_util/Tokenizer.h
	g++ -std=c++11 -o bin/br_ufes_inf_nemo_cpp_util/Tokenizer.o -c src/br_ufes_inf_nemo_cpp_util/Tokenizer.cpp


#
# Classes de domínio:
#
PACOTE_DOMINIO=bin/br_ufes_inf_prog3_trab20152/dominio/Emprestimo.o bin/br_ufes_inf_prog3_trab20152/dominio/Filme.o bin/br_ufes_inf_prog3_trab20152/dominio/Genero.o bin/br_ufes_inf_prog3_trab20152/dominio/Livro.o bin/br_ufes_inf_prog3_trab20152/dominio/Midia.o bin/br_ufes_inf_prog3_trab20152/dominio/Pessoa.o bin/br_ufes_inf_prog3_trab20152/dominio/Serie.o bin/br_ufes_inf_prog3_trab20152/dominio/Temporada.o bin/br_ufes_inf_prog3_trab20152/dominio/Video.o

bin/br_ufes_inf_prog3_trab20152/dominio:
	mkdir -p bin/br_ufes_inf_prog3_trab20152/dominio

bin/br_ufes_inf_prog3_trab20152/dominio/Emprestimo.o: bin/br_ufes_inf_prog3_trab20152/dominio src/br_ufes_inf_prog3_trab20152/dominio/Emprestimo.cpp src/br_ufes_inf_prog3_trab20152/dominio/Emprestimo.h src/br_ufes_inf_prog3_trab20152/dominio/Midia.h
	g++ -std=c++11 -o bin/br_ufes_inf_prog3_trab20152/dominio/Emprestimo.o -c src/br_ufes_inf_prog3_trab20152/dominio/Emprestimo.cpp

bin/br_ufes_inf_prog3_trab20152/dominio/Filme.o: bin/br_ufes_inf_prog3_trab20152/dominio src/br_ufes_inf_prog3_trab20152/dominio/Filme.cpp src/br_ufes_inf_prog3_trab20152/dominio/Filme.h src/br_ufes_inf_prog3_trab20152/dominio/Pessoa.h src/br_ufes_inf_prog3_trab20152/dominio/Video.h
	g++ -std=c++11 -o bin/br_ufes_inf_prog3_trab20152/dominio/Filme.o -c src/br_ufes_inf_prog3_trab20152/dominio/Filme.cpp

bin/br_ufes_inf_prog3_trab20152/dominio/Genero.o: bin/br_ufes_inf_prog3_trab20152/dominio src/br_ufes_inf_prog3_trab20152/dominio/Genero.cpp src/br_ufes_inf_prog3_trab20152/dominio/Genero.h
	g++ -std=c++11 -o bin/br_ufes_inf_prog3_trab20152/dominio/Genero.o -c src/br_ufes_inf_prog3_trab20152/dominio/Genero.cpp

bin/br_ufes_inf_prog3_trab20152/dominio/Livro.o: bin/br_ufes_inf_prog3_trab20152/dominio src/br_ufes_inf_prog3_trab20152/dominio/Livro.cpp src/br_ufes_inf_prog3_trab20152/dominio/Livro.h src/br_ufes_inf_prog3_trab20152/dominio/Midia.h src/br_ufes_inf_prog3_trab20152/dominio/Pessoa.h
	g++ -std=c++11 -o bin/br_ufes_inf_prog3_trab20152/dominio/Livro.o -c src/br_ufes_inf_prog3_trab20152/dominio/Livro.cpp

bin/br_ufes_inf_prog3_trab20152/dominio/Midia.o: bin/br_ufes_inf_prog3_trab20152/dominio src/br_ufes_inf_prog3_trab20152/dominio/Midia.cpp src/br_ufes_inf_prog3_trab20152/dominio/Midia.h src/br_ufes_inf_prog3_trab20152/dominio/Genero.h src/br_ufes_inf_prog3_trab20152/dominio/Pessoa.h src/br_ufes_inf_prog3_trab20152/dominio/Serie.h
	g++ -std=c++11 -o bin/br_ufes_inf_prog3_trab20152/dominio/Midia.o -c src/br_ufes_inf_prog3_trab20152/dominio/Midia.cpp

bin/br_ufes_inf_prog3_trab20152/dominio/Pessoa.o: bin/br_ufes_inf_prog3_trab20152/dominio src/br_ufes_inf_prog3_trab20152/dominio/Pessoa.cpp src/br_ufes_inf_prog3_trab20152/dominio/Pessoa.h
	g++ -std=c++11 -o bin/br_ufes_inf_prog3_trab20152/dominio/Pessoa.o -c src/br_ufes_inf_prog3_trab20152/dominio/Pessoa.cpp

bin/br_ufes_inf_prog3_trab20152/dominio/Serie.o: bin/br_ufes_inf_prog3_trab20152/dominio src/br_ufes_inf_prog3_trab20152/dominio/Serie.cpp src/br_ufes_inf_prog3_trab20152/dominio/Serie.h
	g++ -std=c++11 -o bin/br_ufes_inf_prog3_trab20152/dominio/Serie.o -c src/br_ufes_inf_prog3_trab20152/dominio/Serie.cpp

bin/br_ufes_inf_prog3_trab20152/dominio/Temporada.o: bin/br_ufes_inf_prog3_trab20152/dominio src/br_ufes_inf_prog3_trab20152/dominio/Temporada.cpp src/br_ufes_inf_prog3_trab20152/dominio/Temporada.h src/br_ufes_inf_prog3_trab20152/dominio/Pessoa.h src/br_ufes_inf_prog3_trab20152/dominio/Serie.h src/br_ufes_inf_prog3_trab20152/dominio/Video.h
	g++ -std=c++11 -o bin/br_ufes_inf_prog3_trab20152/dominio/Temporada.o -c src/br_ufes_inf_prog3_trab20152/dominio/Temporada.cpp

bin/br_ufes_inf_prog3_trab20152/dominio/Video.o: bin/br_ufes_inf_prog3_trab20152/dominio src/br_ufes_inf_prog3_trab20152/dominio/Video.cpp src/br_ufes_inf_prog3_trab20152/dominio/Video.h src/br_ufes_inf_prog3_trab20152/dominio/Midia.h src/br_ufes_inf_prog3_trab20152/dominio/Pessoa.h
	g++ -std=c++11 -o bin/br_ufes_inf_prog3_trab20152/dominio/Video.o -c src/br_ufes_inf_prog3_trab20152/dominio/Video.cpp


#
# Classes de I/O:
#
PACOTE_IO=bin/br_ufes_inf_prog3_trab20152/io/Escritor.o bin/br_ufes_inf_prog3_trab20152/io/Estatisticas.o bin/br_ufes_inf_prog3_trab20152/io/Exceptions.o bin/br_ufes_inf_prog3_trab20152/io/Leitor.o bin/br_ufes_inf_prog3_trab20152/io/ConversorCSV.o bin/br_ufes_inf_prog3_trab20152/io/MidiasPorPessoa.o bin/br_ufes_inf_prog3_trab20152/io/Wishlist.o

bin/br_ufes_inf_prog3_trab20152/io:
	mkdir -p bin/br_ufes_inf_prog3_trab20152/io
	
bin/br_ufes_inf_prog3_trab20152/io/ConversorCSV.o: src/br_ufes_inf_prog3_trab20152/io/ConversorCSV.cpp
	g++ -std=c++11 -o bin/br_ufes_inf_prog3_trab20152/io/ConversorCSV.o -c src/br_ufes_inf_prog3_trab20152/io/ConversorCSV.cpp

bin/br_ufes_inf_prog3_trab20152/io/Escritor.o: bin/br_ufes_inf_prog3_trab20152/io src/br_ufes_inf_prog3_trab20152/io/Escritor.cpp src/br_ufes_inf_prog3_trab20152/io/Escritor.h src/br_ufes_inf_prog3_trab20152/io/Estatisticas.h src/br_ufes_inf_nemo_cpp_util/DateUtils.h src/br_ufes_inf_nemo_cpp_util/NumberUtils.h
	g++ -std=c++11 -o bin/br_ufes_inf_prog3_trab20152/io/Escritor.o -c src/br_ufes_inf_prog3_trab20152/io/Escritor.cpp

bin/br_ufes_inf_prog3_trab20152/io/Estatisticas.o: bin/br_ufes_inf_prog3_trab20152/io src/br_ufes_inf_prog3_trab20152/io/Estatisticas.cpp src/br_ufes_inf_prog3_trab20152/io/Estatisticas.h src/br_ufes_inf_prog3_trab20152/dominio/Genero.h src/br_ufes_inf_prog3_trab20152/dominio/Midia.h src/br_ufes_inf_prog3_trab20152/dominio/Serie.h
	g++ -std=c++11 -o bin/br_ufes_inf_prog3_trab20152/io/Estatisticas.o -c src/br_ufes_inf_prog3_trab20152/io/Estatisticas.cpp

bin/br_ufes_inf_prog3_trab20152/io/Exceptions.o: bin/br_ufes_inf_prog3_trab20152/io src/br_ufes_inf_prog3_trab20152/io/Exceptions.cpp src/br_ufes_inf_prog3_trab20152/io/Exceptions.h 
	g++ -std=c++11 -o bin/br_ufes_inf_prog3_trab20152/io/Exceptions.o -c src/br_ufes_inf_prog3_trab20152/io/Exceptions.cpp

bin/br_ufes_inf_prog3_trab20152/io/Leitor.o: bin/br_ufes_inf_prog3_trab20152/io src/br_ufes_inf_prog3_trab20152/io/Leitor.cpp src/br_ufes_inf_prog3_trab20152/io/Leitor.h src/br_ufes_inf_prog3_trab20152/io/ConversorCSV.cpp src/br_ufes_inf_prog3_trab20152/dominio/Emprestimo.h src/br_ufes_inf_prog3_trab20152/dominio/Filme.h src/br_ufes_inf_prog3_trab20152/dominio/Genero.h src/br_ufes_inf_prog3_trab20152/dominio/Midia.h src/br_ufes_inf_prog3_trab20152/dominio/Pessoa.h src/br_ufes_inf_prog3_trab20152/dominio/Serie.h src/br_ufes_inf_prog3_trab20152/dominio/Temporada.h src/br_ufes_inf_nemo_cpp_util/DateUtils.h src/br_ufes_inf_nemo_cpp_util/NumberUtils.h src/br_ufes_inf_nemo_cpp_util/Tokenizer.h
	g++ -std=c++11 -o bin/br_ufes_inf_prog3_trab20152/io/Leitor.o -c src/br_ufes_inf_prog3_trab20152/io/Leitor.cpp

bin/br_ufes_inf_prog3_trab20152/io/MidiasPorPessoa.o: bin/br_ufes_inf_prog3_trab20152/io src/br_ufes_inf_prog3_trab20152/io/MidiasPorPessoa.cpp src/br_ufes_inf_prog3_trab20152/io/MidiasPorPessoa.h src/br_ufes_inf_prog3_trab20152/dominio/Midia.h src/br_ufes_inf_prog3_trab20152/dominio/Pessoa.h
	g++ -std=c++11 -o bin/br_ufes_inf_prog3_trab20152/io/MidiasPorPessoa.o -c src/br_ufes_inf_prog3_trab20152/io/MidiasPorPessoa.cpp

bin/br_ufes_inf_prog3_trab20152/io/Wishlist.o: bin/br_ufes_inf_prog3_trab20152/io src/br_ufes_inf_prog3_trab20152/io/Wishlist.cpp src/br_ufes_inf_prog3_trab20152/io/Wishlist.h src/br_ufes_inf_prog3_trab20152/dominio/Midia.h
	g++ -std=c++11 -o bin/br_ufes_inf_prog3_trab20152/io/Wishlist.o -c src/br_ufes_inf_prog3_trab20152/io/Wishlist.cpp


#
# Aplicação e programa principal:
#
bin/br_ufes_inf_prog3_trab20152:
	mkdir -p bin/br_ufes_inf_prog3_trab20152

bin/br_ufes_inf_prog3_trab20152/AplInveMid.o: bin/br_ufes_inf_prog3_trab20152 src/br_ufes_inf_prog3_trab20152/AplInveMid.cpp src/br_ufes_inf_prog3_trab20152/AplInveMid.h src/br_ufes_inf_prog3_trab20152/io/Escritor.h src/br_ufes_inf_prog3_trab20152/io/Estatisticas.h src/br_ufes_inf_prog3_trab20152/io/Leitor.h
	g++ -std=c++11 -o bin/br_ufes_inf_prog3_trab20152/AplInveMid.o -c src/br_ufes_inf_prog3_trab20152/AplInveMid.cpp

main: ${PACOTE_UTILS} ${PACOTE_DOMINIO} ${PACOTE_IO} src/br_ufes_inf_prog3_trab20152/AplInveMid.h bin/br_ufes_inf_prog3_trab20152/AplInveMid.o src/main.cpp
	g++ -std=c++11 -o main src/main.cpp ${PACOTE_UTILS} ${PACOTE_DOMINIO} ${PACOTE_IO} bin/br_ufes_inf_prog3_trab20152/AplInveMid.o