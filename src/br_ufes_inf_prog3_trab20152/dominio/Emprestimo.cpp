/*
 * Emprestimo.cpp
 *
 *  Created on: Nov 30, 2015
 *      Author: vitor
 */

#include "Emprestimo.h"

namespace br_ufes_inf_prog3_trab20152_dominio {

Emprestimo::Emprestimo(Midia* midia, string tomador, time_t dataEmprestimo, time_t dataDevolucao) {
	this->midia = midia;
	this->tomador = tomador;
	this->dataEmprestimo = dataEmprestimo;
	this->dataDevolucao = dataDevolucao;
}

Midia* Emprestimo::getMidia() const {
	return midia;
}

string Emprestimo::getTomador() const {
	return tomador;
}

time_t Emprestimo::getDataEmprestimo() const {
	return dataEmprestimo;
}

time_t Emprestimo::getDataDevolucao() const {
	return dataDevolucao;
}

ostream& operator<<(ostream& out, const Emprestimo& obj) {
	return cout << "Emprestimo: " << *obj.midia << " -> " << obj.tomador;
}

bool comparaEmprestimos(const Emprestimo* esq, const Emprestimo* dir) {
	int diff = difftime(esq->getDataEmprestimo(), dir->getDataEmprestimo());
	if (diff != 0) return (esq != dir) && (diff >= 0);
	return stringCompare(esq->getTomador(), dir->getTomador());
}

} /* namespace br_ufes_inf_prog3_trab20152_dominio */
