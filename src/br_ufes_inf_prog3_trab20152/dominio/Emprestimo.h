/*
 * Emprestimo.h
 *
 *  Created on: Nov 30, 2015
 *      Author: vitor
 */

#ifndef BR_UFES_INF_PROG3_TRAB20152_DOMINIO_EMPRESTIMO_H_
#define BR_UFES_INF_PROG3_TRAB20152_DOMINIO_EMPRESTIMO_H_

#include <ctime>
#include <iostream>
#include <string>
#include "Midia.h"
#include "../../br_ufes_inf_nemo_cpp_util/StringUtils.h"
using namespace std;
using namespace br_ufes_inf_nemo_cpp_util;

namespace br_ufes_inf_prog3_trab20152_dominio {

class Emprestimo {
	Midia* midia;
	string tomador;
	time_t dataEmprestimo;
	time_t dataDevolucao;

public:
	Emprestimo(Midia* midia, string tomador, time_t dataEmprestimo, time_t dataDevolucao);

	Midia* getMidia() const;
	string getTomador() const;
	time_t getDataEmprestimo() const;
	time_t getDataDevolucao() const;

	friend ostream& operator<<(ostream& out, const Emprestimo& obj);
};

bool comparaEmprestimos(const Emprestimo* esq, const Emprestimo* dir);

} /* namespace br_ufes_inf_prog3_trab20152_dominio */

#endif /* BR_UFES_INF_PROG3_TRAB20152_DOMINIO_EMPRESTIMO_H_ */
