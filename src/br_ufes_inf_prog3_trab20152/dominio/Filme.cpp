/*
 * Filme.cpp
 *
 *  Created on: Nov 27, 2015
 *      Author: vitor
 */

#include "Filme.h"

namespace br_ufes_inf_prog3_trab20152_dominio {

Filme::Filme(int codigo, string nome, int tamanho, double preco, bool possui, bool consumiu, bool deseja, Genero* genero, Pessoa* diretor):
	Video(codigo, nome, tamanho, preco, possui, consumiu, deseja, genero) {
	this->diretor = diretor;
	pessoas = new vector<Pessoa*>();
	pessoas->push_back(diretor);
}

Filme::~Filme() {
	delete pessoas;
}

void Filme::adicionarAtor(Pessoa* ator) {
	Video::adicionarAtor(ator);
	pessoas->push_back(ator);
}

vector<Pessoa*>* Filme::getPessoas() const {
	return pessoas;
}

string Filme::getTipo() const {
	return "Filme";
}

} /* namespace br_ufes_inf_prog3_trab20152_dominio */
