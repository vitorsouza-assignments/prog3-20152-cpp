/*
 * Filme.h
 *
 *  Created on: Nov 27, 2015
 *      Author: vitor
 */

#ifndef BR_UFES_INF_PROG3_TRAB20152_DOMINIO_FILME_H_
#define BR_UFES_INF_PROG3_TRAB20152_DOMINIO_FILME_H_

#include <iostream>
#include <string>
#include <vector>
#include "Pessoa.h"
#include "Video.h"
using namespace std;

namespace br_ufes_inf_prog3_trab20152_dominio {

class Filme: public Video {
	Pessoa* diretor;
	vector<Pessoa*>* pessoas;

public:
	Filme(int codigo, string nome, int tamanho, double preco, bool possui, bool consumiu, bool deseja, Genero* genero, Pessoa* diretor);
	virtual ~Filme();

	virtual void adicionarAtor(Pessoa* ator);
	virtual vector<Pessoa*>* getPessoas() const;
	virtual string getTipo() const;
};

} /* namespace br_ufes_inf_prog3_trab20152_dominio */

#endif /* BR_UFES_INF_PROG3_TRAB20152_DOMINIO_FILME_H_ */
