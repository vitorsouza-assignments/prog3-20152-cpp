/*
 * Genero.cpp
 *
 *  Created on: Nov 25, 2015
 *      Author: vitor
 */

#include "Genero.h"

namespace br_ufes_inf_prog3_trab20152_dominio {

Genero::Genero(string codigo, string nome) {
	this->codigo = codigo;
	this->nome = nome;
}

string Genero::getCodigo() const {
	return codigo;
}

string Genero::getNome() const {
	return nome;
}

ostream& operator<<(ostream& out, const Genero& obj) {
	return out << obj.nome;
}

bool operator==(const Genero& esq, const Genero& dir) {
	return esq.codigo == dir.codigo;
}

} /* namespace ex6 */
