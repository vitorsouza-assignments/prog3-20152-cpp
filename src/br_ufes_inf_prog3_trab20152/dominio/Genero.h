/*
 * Genero.h
 *
 *  Created on: Nov 25, 2015
 *      Author: vitor
 */

#ifndef BR_UFES_INF_PROG3_TRAB20152_DOMINIO_GENERO_H_
#define BR_UFES_INF_PROG3_TRAB20152_DOMINIO_GENERO_H_

#include <iostream>
#include <string>
using namespace std;

namespace br_ufes_inf_prog3_trab20152_dominio {

class Genero {
	string codigo;
	string nome;

public:
	Genero(string codigo, string nome);

	string getCodigo() const;
	string getNome() const;

	friend ostream& operator<<(ostream& out, const Genero& obj);
	friend bool operator==(const Genero& esq, const Genero& dir);
};

} /* namespace ex6 */

#endif /* BR_UFES_INF_PROG3_TRAB20152_DOMINIO_GENERO_H_ */
