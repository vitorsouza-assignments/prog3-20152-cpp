/*
 * Livro.cpp
 *
 *  Created on: Nov 29, 2015
 *      Author: vitor
 */

#include "Livro.h"

namespace br_ufes_inf_prog3_trab20152_dominio {

Livro::Livro(int codigo, string nome, int tamanho, double preco, bool possui, bool consumiu, bool deseja, Genero* genero, vector<Pessoa*>& autores):
	Midia(codigo, nome, tamanho, preco, possui, consumiu, deseja, genero) {
	this->autores = new vector<Pessoa*>();
	*(this->autores) = autores;		// addAll().
}

Livro::~Livro() {
	delete autores;
}

int Livro::calcularHoras() const {
	return 0;
}

vector<Pessoa*>* Livro::getPessoas() const {
	return autores;
}

string Livro::getTipo() const {
	return "Livro";
}

} /* namespace br_ufes_inf_prog3_trab20152_dominio */
