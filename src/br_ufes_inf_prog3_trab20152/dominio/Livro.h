/*
 * Livro.h
 *
 *  Created on: Nov 29, 2015
 *      Author: vitor
 */

#ifndef BR_UFES_INF_PROG3_TRAB20152_DOMINIO_LIVRO_H_
#define BR_UFES_INF_PROG3_TRAB20152_DOMINIO_LIVRO_H_

#include <string>
#include <vector>
#include "Midia.h"
#include "Pessoa.h"
using namespace std;

namespace br_ufes_inf_prog3_trab20152_dominio {

class Livro: public Midia {
	vector<Pessoa*>* autores;

public:
	Livro(int codigo, string nome, int tamanho, double preco, bool possui, bool consumiu, bool deseja, Genero* genero, vector<Pessoa*>& autores);
	~Livro();

	virtual int calcularHoras() const;
	virtual vector<Pessoa*>* getPessoas() const;
	virtual string getTipo() const;
};

} /* namespace br_ufes_inf_prog3_trab20152_dominio */

#endif /* BR_UFES_INF_PROG3_TRAB20152_DOMINIO_LIVRO_H_ */
