/*
 * Midia.cpp
 *
 *  Created on: Nov 27, 2015
 *      Author: vitor
 */

#include "Midia.h"

namespace br_ufes_inf_prog3_trab20152_dominio {

Midia::Midia(int codigo, string nome, int tamanho, double preco, bool possui, bool consumiu, bool deseja, Genero* genero) {
	this->codigo = codigo;
	this->nome = nome;
	this->tamanho = tamanho;
	this->preco = preco;
	this->possui = possui;
	this->consumiu = consumiu;
	this->deseja = deseja;
	this->genero = genero;
}

int Midia::getCodigo() const {
	return codigo;
}

string Midia::getNome() const {
	return nome;
}

int Midia::getTamanho() const {
	return tamanho;
}

double Midia::getPreco() const {
	return preco;
}

Genero* Midia::getGenero() const {
	return genero;
}

bool Midia::isConsumiu() const {
	return consumiu;
}

bool Midia::isDeseja() const {
	return deseja;
}

bool Midia::isPossui() const {
	return possui;
}

bool MidiaComparator::operator()(const Midia* esq, const Midia* dir) const {
	return (esq != dir) && stringCompare(esq->nome, dir->nome);
}

ostream& operator<<(ostream& out, const Midia& obj) {
	return out << obj.nome;
}

} /* namespace br_ufes_inf_prog3_trab20152_dominio */
