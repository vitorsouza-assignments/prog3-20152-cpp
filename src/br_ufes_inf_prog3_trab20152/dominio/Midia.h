/*
 * Midia.h
 *
 *  Created on: Nov 27, 2015
 *      Author: vitor
 */

#ifndef BR_UFES_INF_PROG3_TRAB20152_DOMINIO_MIDIA_H_
#define BR_UFES_INF_PROG3_TRAB20152_DOMINIO_MIDIA_H_

#include <iostream>
#include <string>
#include <vector>
#include "Genero.h"
#include "Pessoa.h"
#include "Serie.h"
#include "../../br_ufes_inf_nemo_cpp_util/StringUtils.h"
using namespace std;
using namespace br_ufes_inf_nemo_cpp_util;

namespace br_ufes_inf_prog3_trab20152_dominio {

class Midia {
	int codigo;
	string nome;
	double preco;
	bool possui;
	bool consumiu;
	bool deseja;
	Genero* genero;

protected:
	int tamanho;

public:
	Midia(int codigo, string nome, int tamanho, double preco, bool possui, bool consumiu, bool deseja, Genero* genero);
	virtual ~Midia() { }

	int getCodigo() const;
	string getNome() const;
	int getTamanho() const;
	double getPreco() const;
	Genero* getGenero() const;
	bool isConsumiu() const;
	bool isDeseja() const;
	bool isPossui() const;

	virtual int calcularHoras() const = 0;
	virtual Serie* getSerie() const { return nullptr; };
	virtual vector<Pessoa*>* getPessoas() const = 0;
	virtual string getTipo() const = 0;

	friend ostream& operator<<(ostream& out, const Midia& obj);
	friend class MidiaComparator;
};

class MidiaComparator {
public:
	bool operator()(const Midia* esq, const Midia* dir) const;
};


} /* namespace br_ufes_inf_prog3_trab20152_dominio */

#endif /* BR_UFES_INF_PROG3_TRAB20152_DOMINIO_MIDIA_H_ */
