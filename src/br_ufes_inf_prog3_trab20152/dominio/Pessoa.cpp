/*
 * Pessoa.cpp
 *
 *  Created on: Nov 26, 2015
 *      Author: vitor
 */

#include "Pessoa.h"

namespace br_ufes_inf_prog3_trab20152_dominio {

Pessoa::Pessoa(int codigo, string nome) {
	this->codigo = codigo;
	this->nome = nome;
}

int Pessoa::getCodigo() const {
	return codigo;
}

ostream& operator<<(ostream& out, const Pessoa& obj) {
	return out << obj.nome;
}

bool operator==(const Pessoa& esq, const Pessoa& dir) {
	return esq.codigo == dir.codigo;
}

bool PessoaComparator::operator()(const Pessoa* esq, const Pessoa* dir) const {
	return (esq != dir) && stringCompare(esq->nome, dir->nome);
}

} /* namespace br_ufes_inf_prog3_trab20152_dominio */
