/*
 * Pessoa.h
 *
 *  Created on: Nov 26, 2015
 *      Author: vitor
 */

#ifndef BR_UFES_INF_PROG3_TRAB20152_DOMINIO_PESSOA_H_
#define BR_UFES_INF_PROG3_TRAB20152_DOMINIO_PESSOA_H_

#include <iostream>
#include <string>
#include "../../br_ufes_inf_nemo_cpp_util/StringUtils.h"
using namespace std;
using namespace br_ufes_inf_nemo_cpp_util;

namespace br_ufes_inf_prog3_trab20152_dominio {

class Pessoa {
	int codigo;
	string nome;

public:
	Pessoa(int codigo, string nome);

	int getCodigo() const;

	friend ostream& operator<<(ostream& out, const Pessoa& obj);
	friend bool operator==(const Pessoa& esq, const Pessoa& dir);
	friend class PessoaComparator;
};

class PessoaComparator {
public:
	bool operator()(const Pessoa* esq, const Pessoa* dir) const;
};

} /* namespace br_ufes_inf_prog3_trab20152_dominio */

#endif /* BR_UFES_INF_PROG3_TRAB20152_DOMINIO_PESSOA_H_ */
