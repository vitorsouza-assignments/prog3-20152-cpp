/*
 * Serie.cpp
 *
 *  Created on: Nov 27, 2015
 *      Author: vitor
 */

#include "Serie.h"

namespace br_ufes_inf_prog3_trab20152_dominio {

Serie::Serie(string nome, Temporada* temporada) {
	this->nome = nome;
	temporadas = new set<Temporada*>();
	temporadas->insert(temporada);
}

Serie::~Serie() {
	delete temporadas;
}

string Serie::getNome() const {
	return nome;
}

ostream& operator<<(ostream& out, const Serie& obj) {
	return out << obj.nome;
}

bool operator==(const Serie& esq, const Serie& dir) {
	return esq.nome.compare(dir.nome) == 0;
}
} /* namespace br_ufes_inf_prog3_trab20152_dominio */
