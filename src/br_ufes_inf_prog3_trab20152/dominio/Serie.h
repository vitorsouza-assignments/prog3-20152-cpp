/*
 * Serie.h
 *
 *  Created on: Nov 27, 2015
 *      Author: vitor
 */

#ifndef BR_UFES_INF_PROG3_TRAB20152_DOMINIO_SERIE_H_
#define BR_UFES_INF_PROG3_TRAB20152_DOMINIO_SERIE_H_

#include <iostream>
#include <set>
#include <string>
using namespace std;

namespace br_ufes_inf_prog3_trab20152_dominio {

class Temporada;

class Serie {
	string nome;
	set<Temporada*>* temporadas;

public:
	Serie(string nome, Temporada* temporada);
	~Serie();

	string getNome() const;

	friend ostream& operator<<(ostream& out, const Serie& obj);
	friend bool operator==(const Serie& esq, const Serie& dir);
};

} /* namespace br_ufes_inf_prog3_trab20152_dominio */

#endif /* BR_UFES_INF_PROG3_TRAB20152_DOMINIO_SERIE_H_ */
