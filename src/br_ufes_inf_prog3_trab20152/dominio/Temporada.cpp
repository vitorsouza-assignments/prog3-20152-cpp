/*
 * Temporada.cpp
 *
 *  Created on: Nov 27, 2015
 *      Author: vitor
 */

#include "Temporada.h"

namespace br_ufes_inf_prog3_trab20152_dominio {

Temporada::Temporada(int codigo, string nome, int tamanho, double preco, bool possui, bool consumiu, bool deseja, Genero* genero, string nomeSerie):
	Video(codigo, nome, tamanho, preco, possui, consumiu, deseja, genero) {
	serie = new Serie(nomeSerie, this);
}

Temporada::Temporada(int codigo, string nome, int tamanho, double preco, bool possui, bool consumiu, bool deseja, Genero* genero, Serie* serie):
	Video(codigo, nome, tamanho, preco, possui, consumiu, deseja, genero) {
	this->serie = serie;
}

Serie* Temporada::getSerie() const {
	return serie;
}

vector<Pessoa*>* Temporada::getPessoas() const {
	return atores;
}

string Temporada::getTipo() const {
	return "Série";
}

} /* namespace br_ufes_inf_prog3_trab20152_dominio */
