/*
 * Temporada.h
 *
 *  Created on: Nov 27, 2015
 *      Author: vitor
 */

#ifndef BR_UFES_INF_PROG3_TRAB20152_DOMINIO_TEMPORADA_H_
#define BR_UFES_INF_PROG3_TRAB20152_DOMINIO_TEMPORADA_H_

#include <iostream>
#include <string>
#include <vector>
#include "Pessoa.h"
#include "Serie.h"
#include "Video.h"
using namespace std;

namespace br_ufes_inf_prog3_trab20152_dominio {

class Temporada: public Video {
	Serie* serie;

public:
	Temporada(int codigo, string nome, int tamanho, double preco, bool possui, bool consumiu, bool deseja, Genero* genero, string nomeSerie);
	Temporada(int codigo, string nome, int tamanho, double preco, bool possui, bool consumiu, bool deseja, Genero* genero, Serie* serie);

	Serie* getSerie() const;
	virtual vector<Pessoa*>* getPessoas() const;
	virtual string getTipo() const;
};

} /* namespace br_ufes_inf_prog3_trab20152_dominio */

#endif /* BR_UFES_INF_PROG3_TRAB20152_DOMINIO_TEMPORADA_H_ */
