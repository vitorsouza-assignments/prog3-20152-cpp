/*
 * Video.cpp
 *
 *  Created on: Nov 27, 2015
 *      Author: vitor
 */

#include "Video.h"

namespace br_ufes_inf_prog3_trab20152_dominio {

Video::Video(int codigo, string nome, int tamanho, double preco, bool possui, bool consumiu, bool deseja, Genero* genero):
	Midia(codigo, nome, tamanho, preco, possui, consumiu, deseja, genero) {
	atores = new vector<Pessoa*>();
}

Video::~Video() {
	delete atores;
}

int Video::calcularHoras() const {
	return tamanho;
}

void Video::adicionarAtor(Pessoa* ator) {
	atores->push_back(ator);
}

} /* namespace br_ufes_inf_prog3_trab20152_dominio */
