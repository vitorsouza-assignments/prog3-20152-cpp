/*
 * Video.h
 *
 *  Created on: Nov 27, 2015
 *      Author: vitor
 */

#ifndef BR_UFES_INF_PROG3_TRAB20152_DOMINIO_VIDEO_H_
#define BR_UFES_INF_PROG3_TRAB20152_DOMINIO_VIDEO_H_

#include <iostream>
#include <string>
#include <vector>
#include "Midia.h"
#include "Pessoa.h"
using namespace std;

namespace br_ufes_inf_prog3_trab20152_dominio {

class Video: public Midia {
protected:
	vector<Pessoa*>* atores;

public:
	Video(int codigo, string nome, int tamanho, double preco, bool possui, bool consumiu, bool deseja, Genero* genero);
	virtual ~Video();

	virtual int calcularHoras() const;
	virtual void adicionarAtor(Pessoa* ator);
};

} /* namespace br_ufes_inf_prog3_trab20152_dominio */

#endif /* BR_UFES_INF_PROG3_TRAB20152_DOMINIO_VIDEO_H_ */
