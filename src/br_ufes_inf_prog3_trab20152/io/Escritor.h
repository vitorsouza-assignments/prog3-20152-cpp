/*
 * Escritor.h
 *
 *  Created on: Dec 2, 2015
 *      Author: vitor
 */

#ifndef BR_UFES_INF_PROG3_TRAB20152_IO_ESCRITOR_H_
#define BR_UFES_INF_PROG3_TRAB20152_IO_ESCRITOR_H_

#include <algorithm>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <map>
#include "Estatisticas.h"
#include "MidiasPorPessoa.h"
#include "Wishlist.h"
#include "../dominio/Emprestimo.h"
#include "../dominio/Midia.h"
#include "../dominio/Pessoa.h"
#include "../../br_ufes_inf_nemo_cpp_util/DateUtils.h"
#include "../../br_ufes_inf_nemo_cpp_util/NumberUtils.h"
using namespace std;
using namespace br_ufes_inf_prog3_trab20152_dominio;
using namespace br_ufes_inf_nemo_cpp_util;


namespace br_ufes_inf_prog3_trab20152_io {

class Escritor {
public:
	void escreverRelatorioEstatisticas(Estatisticas& stats) const;
	void escreverRelatorioMidiasPorPessoa(MidiasPorPessoa& mpp) const;
	void escreverRelatorioEmprestimos(vector<Emprestimo*> emprestimos) const;
	void escreverRelatorioWishlist(Wishlist& wishlist) const;
};

} /* namespace br_ufes_inf_prog3_trab20152_io */

#endif /* BR_UFES_INF_PROG3_TRAB20152_IO_ESCRITOR_H_ */
