/*
 * Leitor.cpp
 *
 *  Created on: Nov 30, 2015
 *      Author: vitor
 */

#include "Leitor.h"

namespace br_ufes_inf_prog3_trab20152_io {

Leitor::Leitor(string nomeArquivoGeneros, string nomeArquivoPessoas, string nomeArquivoMidias, string nomeArquivoEmprestimos) {
	lerGeneros(nomeArquivoGeneros);
	lerPessoas(nomeArquivoPessoas);
	lerMidias(nomeArquivoMidias);
	lerEmprestimos(nomeArquivoEmprestimos);
}

map<string, Genero*> Leitor::getGeneros() const {
	return generos;
}

map<int, Pessoa*> Leitor::getPessoas() const {
	return pessoas;
}

map<int, Midia*> Leitor::getMidias() const {
	return midias;
}

map<string, Serie*> Leitor::getSeries() const {
	return series;
}

vector<Emprestimo*> Leitor::getEmprestimos() const {
	return emprestimos;
}

void Leitor::lerGeneros(string& nomeArquivoGeneros) {
	vector<Genero*> lista;
	ConversorCSVGenero conversor(this);
	conversor.lerArquivo(nomeArquivoGeneros, lista);
	for (Genero* g : lista)
		generos.insert(pair<string, Genero*>(g->getCodigo(), g));
}

void ConversorCSVGenero::criarObjetoDeLinhaCSV(vector<string>& dados, vector<Genero*>& lista) const {
	Genero* genero = new Genero(dados[0], dados[1]);
	lista.push_back(genero);
}

void Leitor::lerPessoas(string& nomeArquivoPessoas) {
	vector<Pessoa*> lista;
	ConversorCSVPessoa conversor(this);
	conversor.lerArquivo(nomeArquivoPessoas, lista);
	for (Pessoa* p : lista)
		pessoas.insert(pair<int, Pessoa*>(p->getCodigo(), p));
}

void ConversorCSVPessoa::criarObjetoDeLinhaCSV(vector<string>& dados, vector<Pessoa*>& lista) const {
	if (! isNumber(dados.at(0))) throw FormatacaoException();
	Pessoa* pessoa = new Pessoa(stoi(dados.at(0)), dados[1]);
	lista.push_back(pessoa);
}

void Leitor::lerMidias(string& nomeArquivoMidias) {
	vector<Midia*> lista;
	ConversorCSVMidia conversor(this);
	conversor.lerArquivo(nomeArquivoMidias, lista);
	for (Midia* m : lista)
		midias.insert(pair<int, Midia*>(m->getCodigo(), m));
}

vector<Pessoa*> recuperarPessoas(string& codigos, map<int, Pessoa*>& pessoas) {
	Tokenizer tok(codigos, ',');
	vector<string> vec = tok.remaining();
	vector<Pessoa*> lista;
	for (string codigo : vec) {
		int codPessoa = stoi(codigo);
		if (pessoas.count(codPessoa) == 0) throw InconsistenciaException("A(u)tor: " + codigo);
		Pessoa* pessoa = pessoas.at(codPessoa);
		lista.push_back(pessoa);
	}
	return lista;
}

void ConversorCSVMidia::criarObjetoDeLinhaCSV(vector<string>& dados, vector<Midia*>& lista) const {
	Midia* midia = nullptr;

	if (leitor->generos.count(dados[6]) == 0) throw InconsistenciaException("Gênero: " + dados[6]);
	Genero* genero = leitor->generos[dados[6]];

	string cabecalhos[3] = { "Possui?", "Consumiu?", "Deseja?" };
	for (int i = 0; i < 3; i++) {
		int idx = i + 9;
		if (dados.size() > idx && ! (dados.at(idx).empty() || stringCompare(dados.at(idx), "x") == 0))
			throw InconsistenciaException(cabecalhos[i] + ": " + dados.at(idx));
	}

	double preco = (dados.size() > 12) ? parseDouble(dados.at(12), LOCALE_PT_BR) : 0;
	bool possui = (dados.size() > 9 && (dados.at(9).compare("x") == 0 || dados.at(9).compare("X") == 0));
	bool consumiu = (dados.size() > 10 && (dados.at(10).compare("x") == 0 || dados.at(10).compare("X") == 0));
	bool deseja = (dados.size() > 11 && (dados.at(11).compare("x") == 0 || dados.at(11).compare("X") == 0));

	if (dados.at(2).empty() || dados.at(2).size() > 1) throw InconsistenciaException("Tipo: " + dados.at(2));
	char tipo = dados.at(2).at(0);
	switch (tipo) {
	case 'F': case 'f': {
		int codDiretor = stoi(dados.at(3));
		if (leitor->pessoas.count(codDiretor) == 0) throw InconsistenciaException("Diretor: " + dados.at(3));
		Pessoa* diretor = leitor->pessoas.at(codDiretor);
		Filme* filme = new Filme(stoi(dados.at(0)), dados.at(1), stoi(dados.at(5)), preco, possui, consumiu, deseja, genero, diretor);
		midia = filme;

		if (dados.size() < 13 && filme->isDeseja() && ! filme->isPossui())
			throw InconsistenciaException("Preço: " + to_string(preco));

		string atores(dados.at(4));
		if (atores.size() > 0) {
			vector<Pessoa*> pessoas = recuperarPessoas(atores, leitor->pessoas);
			for (Pessoa* pessoa : pessoas) filme->adicionarAtor(pessoa);
		}
		break;
	}

	case 'L': case 'l': {
		vector<Pessoa*> pessoas = recuperarPessoas(dados.at(4), leitor->pessoas);
		midia = new Livro(stoi(dados.at(0)), dados.at(1), stoi(dados.at(5)), preco, possui, consumiu, deseja, genero, pessoas);
		break;
	}

	case 'S': case 's': {
		Temporada* temporada = nullptr;
		if (leitor->series.count(dados.at(7)) > 0) {
			Serie* serie = leitor->series.at(dados.at(7));
			temporada = new Temporada(stoi(dados.at(0)), dados.at(1), stoi(dados.at(5)), preco, possui, consumiu, deseja, genero, serie);
		}
		else {
			temporada = new Temporada(stoi(dados.at(0)), dados.at(1), stoi(dados.at(5)), preco, possui, consumiu, deseja, genero, dados.at(7));
			leitor->series.insert(pair<string, Serie*>(dados.at(7), temporada->getSerie()));
		}
		midia = temporada;

		string atores(dados.at(4));
		if (atores.size() > 0) {
			vector<Pessoa*> pessoas = recuperarPessoas(atores, leitor->pessoas);
			for (Pessoa* pessoa : pessoas) temporada->adicionarAtor(pessoa);
		}
		break;
	}
	default:
		throw InconsistenciaException("Tipo: " + dados.at(2));
	}

	lista.push_back(midia);
}

void Leitor::lerEmprestimos(string& nomeArquivoEmprestimos) {
	ConversorCSVEmprestimo conversor(this);
	conversor.lerArquivo(nomeArquivoEmprestimos, emprestimos);
}

void ConversorCSVEmprestimo::criarObjetoDeLinhaCSV(vector<string>& dados, vector<Emprestimo*>& lista) const {
	int codMidia = stoi(dados.at(0));
	if (leitor->midias.count(codMidia) == 0) throw InconsistenciaException("Mídia: " + dados[0]);
	Midia* midia = leitor->midias.at(codMidia);
	if (! validDate(dados.at(2), DATE_FORMAT_PT_BR_SHORT) || ! validDate(dados.at(3), DATE_FORMAT_PT_BR_SHORT)) throw FormatacaoException();
	Emprestimo* emprestimo = new Emprestimo(midia, dados.at(1), parseDate(dados.at(2), DATE_FORMAT_PT_BR_SHORT), parseDate(dados.at(3), DATE_FORMAT_PT_BR_SHORT));
	lista.push_back(emprestimo);
}


} /* namespace br_ufes_inf_prog3_trab20152_io */
