/*
 * Leitor.h
 *
 *  Created on: Nov 30, 2015
 *      Author: vitor
 */

#ifndef BR_UFES_INF_PROG3_TRAB20152_IO_LEITOR_H_
#define BR_UFES_INF_PROG3_TRAB20152_IO_LEITOR_H_

#include <ctime>
#include <map>
#include <string>
#include <vector>
#include "../dominio/Emprestimo.h"
#include "../dominio/Filme.h"
#include "../dominio/Genero.h"
#include "../dominio/Livro.h"
#include "../dominio/Midia.h"
#include "../dominio/Pessoa.h"
#include "../dominio/Serie.h"
#include "../dominio/Temporada.h"
#include "../../br_ufes_inf_nemo_cpp_util/DateUtils.h"
#include "../../br_ufes_inf_nemo_cpp_util/NumberUtils.h"
#include "../../br_ufes_inf_nemo_cpp_util/Tokenizer.h"
#include "ConversorCSV.cpp"
#include "Exceptions.h"
using namespace std;
using namespace br_ufes_inf_prog3_trab20152_dominio;
using namespace br_ufes_inf_nemo_cpp_util;

namespace br_ufes_inf_prog3_trab20152_io {

class Leitor {
	map<string, Genero*> generos;
	map<int, Pessoa*> pessoas;
	map<int, Midia*> midias;
	map<string, Serie*> series;
	vector<Emprestimo*> emprestimos;

	void lerGeneros(string& nomeArquivoGeneros);
	void lerPessoas(string& nomeArquivoPessoas);
	void lerMidias(string& nomeArquivoMidias);
	void lerEmprestimos(string& nomeArquivoEmprestimos);

public:
	Leitor(string nomeArquivoGeneros, string nomeArquivoPessoas, string nomeArquivoMidias, string nomeArquivoEmprestimos);

	map<string, Genero*> getGeneros() const;
	map<int, Pessoa*> getPessoas() const;
	map<int, Midia*> getMidias() const;
	map<string, Serie*> getSeries() const;
	vector<Emprestimo*> getEmprestimos() const;

	friend class ConversorCSVGenero;
	friend class ConversorCSVPessoa;
	friend class ConversorCSVMidia;
	friend class ConversorCSVEmprestimo;
};


class ConversorCSVGenero: public ConversorCSV<Genero*> {
public:
	ConversorCSVGenero(Leitor* leitor): ConversorCSV<Genero*>(leitor) { }
	void criarObjetoDeLinhaCSV(vector<string>& dados, vector<Genero*>& lista) const;
};

class ConversorCSVPessoa: public ConversorCSV<Pessoa*> {
public:
	ConversorCSVPessoa(Leitor* leitor): ConversorCSV<Pessoa*>(leitor) { }
	void criarObjetoDeLinhaCSV(vector<string>& dados, vector<Pessoa*>& lista) const;
};

class ConversorCSVMidia: public ConversorCSV<Midia*> {
public:
	ConversorCSVMidia(Leitor* leitor): ConversorCSV<Midia*>(leitor) { }
	void criarObjetoDeLinhaCSV(vector<string>& dados, vector<Midia*>& lista) const;
};

class ConversorCSVEmprestimo: public ConversorCSV<Emprestimo*> {
public:
	ConversorCSVEmprestimo(Leitor* leitor): ConversorCSV<Emprestimo*>(leitor) { }
	void criarObjetoDeLinhaCSV(vector<string>& dados, vector<Emprestimo*>& lista) const;
};

} /* namespace br_ufes_inf_prog3_trab20152_io */

#endif /* BR_UFES_INF_PROG3_TRAB20152_IO_LEITOR_H_ */
