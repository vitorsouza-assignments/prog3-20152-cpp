/*
 * MidiasPorPessoa.cpp
 *
 *  Created on: Dec 2, 2015
 *      Author: vitor
 */

#include "MidiasPorPessoa.h"

namespace br_ufes_inf_prog3_trab20152_io {

MidiasPorPessoa::MidiasPorPessoa(map<int, Midia*> midias) {
	for (pair<int, Midia*> par : midias) {
		for (Pessoa* pessoa : *(par.second->getPessoas())) {
			map<Pessoa*, set<Midia*, MidiaComparator>, PessoaComparator>::iterator iter = producoes.find(pessoa);
			if (iter == producoes.end()) {
				set<Midia*, MidiaComparator> producao;
				producao.insert(par.second);
				producoes.insert(pair<Pessoa*, set<Midia*, MidiaComparator> >(pessoa, producao));
			}
			else {
				iter->second.insert(par.second);
			}
		}
	}
}

} /* namespace br_ufes_inf_prog3_trab20152_io */
