/*
 * MidiasPorPessoa.h
 *
 *  Created on: Dec 2, 2015
 *      Author: vitor
 */

#ifndef BR_UFES_INF_PROG3_TRAB20152_IO_MIDIASPORPESSOA_H_
#define BR_UFES_INF_PROG3_TRAB20152_IO_MIDIASPORPESSOA_H_

#include <map>
#include <set>
#include "../dominio/Midia.h"
#include "../dominio/Pessoa.h"
using namespace std;
using namespace br_ufes_inf_prog3_trab20152_dominio;

namespace br_ufes_inf_prog3_trab20152_io {

class MidiasPorPessoa {
	map<Pessoa*, set<Midia*, MidiaComparator>, PessoaComparator> producoes;

public:
	MidiasPorPessoa(map<int, Midia*> midias);

	friend class Escritor;
};

} /* namespace br_ufes_inf_prog3_trab20152_io */

#endif /* BR_UFES_INF_PROG3_TRAB20152_IO_MIDIASPORPESSOA_H_ */
