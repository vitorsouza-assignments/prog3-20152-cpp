/*
 * Wishlist.cpp
 *
 *  Created on: Dec 2, 2015
 *      Author: vitor
 */

#include "Wishlist.h"

namespace br_ufes_inf_prog3_trab20152_io {

Wishlist::Wishlist(map<int, Midia*> midias) {
	for (pair<int, Midia*> par : midias) {
		Midia* midia = par.second;
		if (midia->isDeseja() && ! midia->isPossui())
			itens.insert(new ItemWishlist(midia));
	}
}

Wishlist::~Wishlist() {
	for (ItemWishlist* item : itens) delete item;
}

ItemWishlist::ItemWishlist(Midia *midia) {
	this->midia = midia;
}

bool ItemWishlistComparator::operator()(const ItemWishlist* esq, const ItemWishlist* dir) const {
	if (esq == dir) return false;

	int cmp = esq->midia->getTipo().compare(dir->midia->getTipo());
	if (cmp != 0) return cmp < 0;

	int precoEsq = (int)(esq->midia->getPreco() * 100);
	int precoDir = (int)(dir->midia->getPreco() * 100);
	cmp = precoEsq - precoDir;
	if (cmp != 0) return cmp > 0;

	return esq->midia->getNome() <= dir->midia->getNome();
}

} /* namespace br_ufes_inf_prog3_trab20152_io */
