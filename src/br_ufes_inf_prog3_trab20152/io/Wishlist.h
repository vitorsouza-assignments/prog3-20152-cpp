/*
 * Wishlist.h
 *
 *  Created on: Dec 2, 2015
 *      Author: vitor
 */

#ifndef BR_UFES_INF_PROG3_TRAB20152_IO_WISHLIST_H_
#define BR_UFES_INF_PROG3_TRAB20152_IO_WISHLIST_H_

#include <map>
#include <set>
#include "../dominio/Midia.h"
using namespace std;
using namespace br_ufes_inf_prog3_trab20152_dominio;

namespace br_ufes_inf_prog3_trab20152_io {

class ItemWishlist {
	Midia *midia;

public:
	ItemWishlist(Midia *midia);

	friend class ItemWishlistComparator;
	friend class Escritor;
};

class ItemWishlistComparator {
public:
	bool operator()(const ItemWishlist* esq, const ItemWishlist* dir) const;
};

class Wishlist {
	set<ItemWishlist*, ItemWishlistComparator> itens;

public:
	Wishlist(map<int, Midia*> midias);
	~Wishlist();

	friend class Escritor;
};


} /* namespace br_ufes_inf_prog3_trab20152_io */

#endif /* BR_UFES_INF_PROG3_TRAB20152_IO_WISHLIST_H_ */
