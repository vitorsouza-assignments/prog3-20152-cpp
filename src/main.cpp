/*
 * main.cpp
 *
 *  Created on: Nov 25, 2015
 *      Author: vitor
 */

#include <iostream>
#include <string>
#include "br_ufes_inf_prog3_trab20152/AplInveMid.h"
#include "br_ufes_inf_prog3_trab20152/io/Exceptions.h"

using namespace std;
using namespace br_ufes_inf_prog3_trab20152;
using namespace br_ufes_inf_prog3_trab20152_io;

int main(int argc, char **argv) {
	try {
		string nomeArquivoGeneros, nomeArquivoPessoas, nomeArquivoMidias, nomeArquivoEmprestimos;
		string gFlag("-g"), pFlag("-p"), mFlag("-m"), eFlag("-e");

		for (int i = 0; i < argc; i++) {
			if (gFlag.compare(argv[i]) == 0 && argc > i + 1) nomeArquivoGeneros = argv[i + 1];
			else if (pFlag.compare(argv[i]) == 0 && argc > i + 1) nomeArquivoPessoas = argv[i + 1];
			else if (mFlag.compare(argv[i]) == 0 && argc > i + 1) nomeArquivoMidias = argv[i + 1];
			else if (eFlag.compare(argv[i]) == 0 && argc > i + 1) nomeArquivoEmprestimos = argv[i + 1];
		}

		if (! nomeArquivoGeneros.empty() && ! nomeArquivoPessoas.empty() && ! nomeArquivoMidias.empty() && ! nomeArquivoEmprestimos.empty()) {
			AplInveMid apl;
			apl.lerDados(nomeArquivoGeneros, nomeArquivoPessoas, nomeArquivoMidias, nomeArquivoEmprestimos);
			Escritor escritor;
			apl.escreverRelatorios(escritor);
		}
		else {
			throw IOException();
		}
	}
	catch (InconsistenciaException& e) {
		cout << "Dados inconsistentes (" << e.what() << ")" << endl;
	}
	catch (FormatacaoException&) {
		cout << "Erro de formatação" << endl;
	}
	catch (IOException&) {
		cout << "Erro de I/O" << endl;
	}
}
